/*
 *  
 *  
 *  _________ ____  .-. _________/ ____ .-. ____ 
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'      
 *  
 *  
 *  art & game engine
 *  
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *  
 * 
 *  This file is part of PROJECT_NAME
 *  For the latest info, see http://polymorph.cool/
 *  
 *  Copyright (c) 2019 polymorph.cool
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 *  ___________________________________( ^3^)_____________________________________
 *  
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *  
 * 
 */

/* 
 * File:   futari_server.cpp
 * Author: frankiezafe
 * 
 * Created on January 17, 2019, 11:18 PM
 */

#include "futari_server.h"

static FutariServer* _instance = 0;

FutariServer* FutariServer::get() {
    
    if (_instance == 0) {
        _instance = new FutariServer();
    }
    return _instance;
    
}

void FutariServer::append(FutariModifierData* fm) {

    if ( !fm ) { return; }
    
    get();

    if (std::find(
            _instance->modifier_data.begin(),
            _instance->modifier_data.end(),
            fm
            ) != _instance->modifier_data.end()) {
        return;
    }

    _instance->mut->lock();
    _instance->modifier_data.push_back(fm);
    _instance->_revision++;
    _instance->mut->unlock();

}

void FutariServer::remove(FutariModifierData* fm) {

    if ( !fm ) { return; }
    
    if (!_instance || _instance->modifier_data.empty()) {
        return;
    }

    std::vector< FutariModifierData* >::iterator itm = std::find(
            _instance->modifier_data.begin(),
            _instance->modifier_data.end(),
            fm
            );

    if (itm != _instance->modifier_data.end()) {

        _instance->mut->lock();
        _instance->modifier_data.erase(itm);
        _instance->_revision++;
        _instance->mut->unlock();

    }

}

void FutariServer::lock() {
    if (_instance) {
        _instance->mut->lock();
    }
}

void FutariServer::unlock() {
    if (_instance) {
        _instance->mut->unlock();
    }
}

std::vector< FutariModifierData* >& FutariServer::data() {
    get();
    return _instance->modifier_data;
}

const uint64_t& FutariServer::revision() {
    get();
    return _instance->_revision;
}

FutariServer::FutariServer():
_revision(0)
{
    mut = Mutex::create();
}

FutariServer::~FutariServer() {
    memdelete(mut);
}