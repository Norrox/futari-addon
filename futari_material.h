/*
 *  
 *  
 *  _________ ____  .-. _________/ ____ .-. ____ 
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'      
 *  
 *  
 *  art & game engine
 *  
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *  
 * 
 *  This file is part of PROJECT_NAME
 *  For the latest info, see http://polymorph.cool/
 *  
 *  Copyright (c) 2019 polymorph.cool
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 *  ___________________________________( ^3^)_____________________________________
 *  
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *  
 * 
 */

/* 
 * File:   futari_material.h
 * Author: frankiezafe
 *
 * Created on January 7, 2019, 12:40 PM
 */

#ifndef FUTARI_PROCESS_MATERIAL_H
#define FUTARI_PROCESS_MATERIAL_H

#include <iostream>
#include <string>
#include <sstream>

#include "core/rid.h"
#include "scene/resources/material.h"
#include "scene/resources/particles_material.h"

#include "futari_wind.h"

class FutariShaderPart {
public:

    int modifier_count;
    
    std::stringstream ss_declaration;
    std::stringstream ss_vertex_start;
    std::stringstream ss_vertex_normal;
    
    String declaration;
    String vertex_start;
    String vertex_normal;

    FutariShaderPart( ) : modifier_count( 0 ) {
    }

    void reset( ) {
        modifier_count = 0;
        
        ss_declaration.str("");
        ss_vertex_start.str("");
        ss_vertex_normal.str("");
        
        declaration.clear( );
        vertex_start.clear( );
        vertex_normal.clear( );
    }
    
    void pack() {
        declaration = ss_declaration.str().c_str();
        vertex_start = ss_vertex_start.str().c_str();
        vertex_normal = ss_vertex_normal.str().c_str();
    }

    bool empty( ) const {
        return (
                modifier_count == 0 &&
                declaration.empty( ) &&
                vertex_start.empty( ) &&
                vertex_normal.empty( ) );
    }

    void operator=( const FutariShaderPart& src ) {
        modifier_count = src.modifier_count;
        declaration = src.declaration;
        vertex_start = src.vertex_start;
        vertex_normal = src.vertex_normal;
    }

    bool operator==( const FutariShaderPart& src ) const {
        return (
                modifier_count == src.modifier_count &&
                declaration == src.declaration &&
                vertex_start == src.vertex_start &&
                vertex_normal == src.vertex_normal
                );
    }

    bool operator!=( const FutariShaderPart& src ) const {
        return (
                modifier_count != src.modifier_count ||
                declaration != src.declaration ||
                vertex_start != src.vertex_start ||
                vertex_normal != src.vertex_normal
                );
    }
    
    void print() const {
        std::cout << 
                "modifier_count: " << modifier_count << std::endl <<
                "declaration: '" << ss_declaration.str() << "'" << std::endl <<
                "vertex_start: '" << ss_vertex_start.str() << "'" << std::endl <<
                "vertex_normal: '" << ss_vertex_normal.str() << "'" << std::endl;
    }

};

class FutariMaterial : public Material {

    GDCLASS( FutariMaterial, Material )

public:
    enum FParameter {
        FUPA_INITIAL_LINEAR_VELOCITY,
        FUPA_ANGULAR_VELOCITY,
        FUPA_ORBIT_VELOCITY,
        FUPA_LINEAR_ACCEL,
        FUPA_RADIAL_ACCEL,
        FUPA_TANGENTIAL_ACCEL,
        FUPA_DAMPING,
        FUPA_ANGLE,
        FUPA_ANIM_SPEED,
        FUPA_ANIM_OFFSET,
        FUPA_MAX
    };

    enum FFlags {
        FUFLA_ALIGN_Y_TO_VELOCITY,
        FUFLA_ROTATE_Y,
        FUFLA_DISABLE_Z,
        FUFLA_ENABLE_WIND,
        FUFLA_ENABLE_ATTRACTOR,
        FUFLA_ENABLE_VORTEX,
        FUFLA_MESSY_ORIENTATION,
        FUFLA_ROTATE_X,
        FUFLA_ROTATE_Z,
        FUFLA_ENABLE_FLOOR,
        FUFLA_MAX
    };

    enum FEmissionShape {
        FUEM_SHAPE_POINT,
        FUEM_SHAPE_SPHERE,
        FUEM_SHAPE_CYLINDER,
        FUEM_SHAPE_BOX,
        FUEM_SHAPE_POINTS,
        FUEM_SHAPE_DIRECTED_POINTS,
    };


protected:

    static Mutex *material_mutex;
    static SelfList<FutariMaterial>::List *dirty_materials;

    struct ShaderNames {
        StringName spread;
        StringName flatness;
        StringName initial_linear_velocity;
        StringName initial_angle;
        StringName angular_velocity;
        StringName orbit_velocity;
        StringName linear_accel;
        StringName radial_accel;
        StringName tangent_accel;
        StringName damping;
        StringName scale;
        StringName anim_speed;
        StringName anim_offset;
        
        StringName angle_x_mult;
        StringName angle_y_mult;
        StringName angle_z_mult;

        StringName initial_linear_velocity_random;
        StringName initial_angle_random;
        StringName angular_velocity_random;
        StringName orbit_velocity_random;
        StringName linear_accel_random;
        StringName radial_accel_random;
        StringName tangent_accel_random;
        StringName damping_random;
        StringName scale_random;
        StringName anim_speed_random;
        StringName anim_offset_random;

        StringName angle_texture;
        StringName angular_velocity_texture;
        StringName orbit_velocity_texture;
        StringName linear_accel_texture;
        StringName radial_accel_texture;
        StringName tangent_accel_texture;
        StringName damping_texture;
        StringName scale_gradient;
        StringName anim_speed_texture;
        StringName anim_offset_texture;

        StringName color;
        StringName color_ramp;

        StringName emission_sphere_radius;
        StringName emission_cylinder_radius;
        StringName emission_cylinder_height;
        StringName emission_box_extents;
        StringName emission_texture_point_count;
        StringName emission_texture_points;
        StringName emission_texture_normal;
        StringName emission_texture_color;

        StringName trail_divisor;
        StringName trail_size_modifier;
        StringName trail_color_modifier;

        StringName gravity;

    };

    static ShaderNames *shader_names;

    SelfList<FutariMaterial> element;

    void _update_shader( );
    _FORCE_INLINE_ void _queue_shader_change( );
    _FORCE_INLINE_ bool _is_shader_dirty( ) const;

    float spread;
    float flatness;

    float parameters[FUPA_MAX];
    float randomness[FUPA_MAX];

    Ref<Texture> tex_parameters[FUPA_MAX];
    Color color;
    Ref<Texture> color_ramp;
    
    float scale;
    float scale_random;
    Ref<Texture> scale_gradient;
    
    float angle_x_mult;
    float angle_y_mult;
    float angle_z_mult;

    bool flags[FUFLA_MAX];

    FEmissionShape emission_shape;
    float emission_sphere_radius;
    float emission_cylinder_radius;
    float emission_cylinder_height;
    Vector3 emission_box_extents;
    Ref<Texture> emission_point_texture;
    Ref<Texture> emission_normal_texture;
    Ref<Texture> emission_color_texture;
    int emission_point_count;

    bool anim_loop;

    int trail_divisor;

    Ref<CurveTexture> trail_size_modifier;
    Ref<GradientTexture> trail_color_modifier;

    Vector3 gravity;

//    int wind_count;
//    int attractor_count;
//    int vortex_count;

    FutariShaderPart common_part;
    FutariShaderPart wind_part;
    FutariShaderPart attractor_part;
    FutariShaderPart vortex_part;
    FutariShaderPart floor_part;

    RID _shader;
    bool dump_shader;

    //do not save emission points here

    static void _bind_methods( );
    virtual void _validate_property( PropertyInfo &property ) const;

public:

    void set_spread( float p_spread );
    float get_spread( ) const;

    void set_flatness( float p_flatness );
    float get_flatness( ) const;

    void set_param( FParameter p_param, float p_value );
    float get_param( FParameter p_param ) const;

    void set_param_randomness( FParameter p_param, float p_value );
    float get_param_randomness( FParameter p_param ) const;

    void set_param_texture( FParameter p_param, const Ref<Texture> &p_texture );
    Ref<Texture> get_param_texture( FParameter p_param ) const;

    void set_color( const Color &p_color );
    Color get_color( ) const;

    void set_color_ramp( const Ref<Texture> &p_texture );
    Ref<Texture> get_color_ramp( ) const;

    void set_scale( const float &p_scale );
    float get_scale( ) const;

    void set_angle_x_mult( const float &p_angle_x_mult );
    float get_angle_x_mult( ) const;

    void set_angle_y_mult( const float &p_angle_y_mult );
    float get_angle_y_mult( ) const;

    void set_angle_z_mult( const float &p_angle_z_mult );
    float get_angle_z_mult( ) const;
    
    void set_scale_random( const float &p_scale_random );
    float get_scale_random( ) const;
    
    void set_scale_gradient( const Ref<Texture> &p_texture );
    Ref<Texture> get_scale_gradient( ) const;

    void set_flag( FFlags p_flag, bool p_enable );
    bool get_flag( FFlags p_flag ) const;

    void set_emission_shape( FEmissionShape p_shape );
    void set_emission_sphere_radius( float p_radius );
    void set_emission_cylinder_radius( float p_radius );
    void set_emission_cylinder_height( float p_height );
    void set_emission_box_extents( Vector3 p_extents );
    void set_emission_point_texture( const Ref<Texture> &p_points );
    void set_emission_normal_texture( const Ref<Texture> &p_normals );
    void set_emission_color_texture( const Ref<Texture> &p_colors );
    void set_emission_point_count( int p_count );

    FEmissionShape get_emission_shape( ) const;
    float get_emission_sphere_radius( ) const;
    float get_emission_cylinder_radius( ) const;
    float get_emission_cylinder_height( ) const;
    Vector3 get_emission_box_extents( ) const;
    Ref<Texture> get_emission_point_texture( ) const;
    Ref<Texture> get_emission_normal_texture( ) const;
    Ref<Texture> get_emission_color_texture( ) const;
    int get_emission_point_count( ) const;

    void set_trail_divisor( int p_divisor );
    int get_trail_divisor( ) const;

    void set_trail_size_modifier( const Ref<CurveTexture> &p_trail_size_modifier );
    Ref<CurveTexture> get_trail_size_modifier( ) const;

    void set_trail_color_modifier( const Ref<GradientTexture> &p_trail_color_modifier );
    Ref<GradientTexture> get_trail_color_modifier( ) const;

    void set_gravity( const Vector3 &p_gravity );
    Vector3 get_gravity( ) const;

    // c++ use only, not exposed in gdscript
    void set_futari_param_real( String name, const real_t& r );
    void set_futari_param_v3( String name, const Vector3& v );
    void set_futari_param_tex( String name, const Ref<Texture> &tex );

    void set_common_shaderpart( const FutariShaderPart& part );
    void set_wind_shaderpart( const FutariShaderPart& part );
    void set_attractor_shaderpart( const FutariShaderPart& part );
    void set_vortex_shaderpart( const FutariShaderPart& part );
    void set_floor_shaderpart( const FutariShaderPart& part );

    static void init_shaders( );
    static void finish_shaders( );
    static void flush_changes( );

    RID get_shader_rid( ) const;
    void set_dump_shader( bool enable );
    bool get_dump_shader( ) const;

    virtual Shader::Mode get_shader_mode( ) const;

    FutariMaterial( );
    ~FutariMaterial( );

};

VARIANT_ENUM_CAST( FutariMaterial::FParameter )
VARIANT_ENUM_CAST( FutariMaterial::FFlags )
VARIANT_ENUM_CAST( FutariMaterial::FEmissionShape )

#endif /* FUTARI_PROCESS_MATERIAL_H */

