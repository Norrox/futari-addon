# Futari addon

Module for [godot engine](https://godotengine.org/) developed for Futari Shizuka, The Maiden from the Sea by Toshio Hosokawa.

Contains classes to interacts with particle systems, such as wind, vortex and localised modifiers.

Developed for latest version of godot engine: requires a recompilation of the engine and invalidates export templates!

## demo project

Check the demo project for this addon here: https://gitlab.com/polymorphcool/futari-addon-demo/

![](https://polymorph.cool/wp-content/uploads/2019/01/futari-addon_particles_wind.png)

## notes about development

Futari objects' communication is not based on signals, due to the complexity of the relation between them.

To pass info from modifiers to particles, modifiers push a reprensentation of themsleves in a FutariServer. This server is a singleton.

* On *Spatial::NOTIFICATION_ENTER_WORLD*, modifier publish a simplified representation of itself in the server. This representation is a struct containing values and modification flags per value. The struct is instanciated in the modifier and the server holds a pointer to it;
* On *Node::NOTIFICATION_INTERNAL_PROCESS* (on each frame), modifier check if any values have changed, depending on its type, update and flag its representation accordingly. The server holding a pointer to the representation, it doesn't need to be accessed or updated;
* One *NOTIFICATION_UNPARENTED*, *NOTIFICATION_EXIT_TREE* & *NOTIFICATION_EXIT_WORLD*, modifier removes its representation form the server.

Attractor, Wind and Vortex inherits from modifier. See code for further details about this.

The server holds an updated representation of all active modifiers in the scene. These representations contain copies of values, such as position or orientation of their modifier. Therefore, there is no risk of a concurrent access while reading these values, especially when using the server mutex (*FutariServer::lock()* & *FutariServer::unlock()*) before accessing it.

Particles' update process is bounded to tree's idle signal via this call: *get_tree()->connect("idle_frame", this, "_internal_process");*. When accessing modifiers' values directly, the code was crashing. The server is a safe way to retrieve info from modifiers without risking a segmentation fault or a concurrent access.

At each idle signal, particles lock the server and read the list of representation available at the time. Depending of the flags of the representations, the particles will update relevant part of the shader or trigger a recompilation if the number or kind of modifiers has vary. During this process, the server is locked by particles.

The update of particles might be long. The code is optimised to do only the necessary updates. Check *FutariParticles::_internal_process()* for more details.

## compilation

### linux

``` bash
    git clone https://github.com/godotengine/godot.git godot
    cd godot/modules
    git clone https://gitlab.com/frankiezafe/futari-addon.git futari
    cd ../../
    scons platform=x11
```

This will take a while the first time, as the full godot engine will be compiled (around 10 / 15 min i would say).

Once done, just go back to godot/modules/futari and type:

``` bash
    git pull
    cd ../../
    scons platform=x11
```

to update the project.