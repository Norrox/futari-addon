/*
 *  
 *  
 *  _________ ____  .-. _________/ ____ .-. ____ 
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'      
 *  
 *  
 *  art & game engine
 *  
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *  
 * 
 *  This file is part of PROJECT_NAME
 *  For the latest info, see http://polymorph.cool/
 *  
 *  Copyright (c) 2019 polymorph.cool
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 *  ___________________________________( ^3^)_____________________________________
 *  
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *  
 * 
 */

/* 
 * File:   futari_attractor.cpp
 * Author: frankiezafe
 * 
 * Created on January 10, 2019, 4:20 PM
 */

#include "futari_attractor.h"

FutariAttractor::FutariAttractor() {
    range = FUTARI_ATTRACTOR_MINRANGE;
    _data.ptr = this;
    _data.futari_type = FutariModifierData::FMT_ATTRACTOR;
}

void FutariAttractor::set_range(real_t r) {
    if (r < FUTARI_ATTRACTOR_MINRANGE) {
        range = FUTARI_ATTRACTOR_MINRANGE;
        return;
    }
    range = r;
#ifdef TOOLS_ENABLED
    update_gizmo();
#endif
}

FutariModifierData* FutariAttractor::data_ptr() {

    return &(FutariModifier::_data);

}

void FutariAttractor::refresh() {

    _data.reset();

    if (!enabled) {
        _data.changed_enabled = _data.enabled != enabled;
        _data.changed = _data.changed_enabled;
        _data.enabled = enabled;
        return;
    }

    Vector3 p = get_global_transform().origin;
    Vector3 o = get_rotation();
    float s = strength;
    if (!is_visible()) {
        s *= 0;
    }

    _data.changed_layer = _data.futari_layers != futari_layers;
    _data.changed_enabled = _data.enabled != enabled;
    _data.changed_position = _data.position != p;
    _data.changed_orientation = _data.orientation != o;
    _data.changed_strength = _data.strength != s;
    _data.changed_strength_decay =
            _data.strength_decay != strength_decay_texture ||
            _data.strength_decay.is_valid() != strength_decay_texture.is_valid();
    _data.changed_lifetime = 
            _data.lifetime != lifetime_texture || 
            _data.lifetime.is_valid() != lifetime_texture.is_valid();
    _data.changed_range = _data.range != range;
    _data.changed_velocity_mult = _data.velocity_mult != velocity_mult;
    _data.changed_position_mult = _data.position_mult != position_mult;

    _data.changed = 
            _data.changed_layer ||
            _data.changed_enabled ||
            _data.changed_position ||
            _data.changed_orientation ||
            _data.changed_strength ||
            _data.changed_strength_decay ||
            _data.changed_lifetime ||
            _data.changed_range ||
            _data.changed_velocity_mult ||
            _data.changed_position_mult;

    if (_data.changed) {
        _data.futari_layers = futari_layers;
        _data.enabled = enabled;
        _data.position = p;
        _data.orientation = o;
        _data.strength = s;
        _data.strength_decay = strength_decay_texture;
        _data.lifetime = lifetime_texture;
        _data.range = range;
        _data.velocity_mult = velocity_mult;
        _data.position_mult = position_mult;
    }

}

void FutariAttractor::_bind_methods() {

    ADD_GROUP("Attractor", "attractor_");
    ADD_PROPERTY(PropertyInfo(Variant::BOOL, "enabled"), "set_enabled", "get_enabled");
    String prop = String::num_real(FUTARI_ATTRACTOR_MINRANGE) + ",100,0.01,or_greater";
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "range", PROPERTY_HINT_RANGE, prop), "set_range", "get_range");
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "strength", PROPERTY_HINT_RANGE, "-500,500,or_greater,or_lesser"), "set_strength", "get_strength");
    ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "strength_decay_texture", PROPERTY_HINT_RESOURCE_TYPE, "GradientTexture"), "set_strength_decay_texture", "get_strength_decay_texture");
    ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "lifetime_texture", PROPERTY_HINT_RESOURCE_TYPE, "GradientTexture"), "set_lifetime_texture", "get_lifetime_texture");
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "velocity_mult", PROPERTY_HINT_RANGE, "0,1"), "set_velocity_mult", "get_velocity_mult");
    ADD_PROPERTY(PropertyInfo(Variant::REAL, "position_mult", PROPERTY_HINT_RANGE, "0,1"), "set_position_mult", "get_position_mult");

}

