/*
 *  
 *  
 *  _________ ____  .-. _________/ ____ .-. ____ 
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'      
 *  
 *  
 *  art & game engine
 *  
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *  
 * 
 *  This file is part of PROJECT_NAME
 *  For the latest info, see http://polymorph.cool/
 *  
 *  Copyright (c) 2019 polymorph.cool
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 *  ___________________________________( ^3^)_____________________________________
 *  
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *  
 * 
 */

/* 
 * File:   futari_particles.h
 * Author: frankiezafe
 *
 * Created on January 6, 2019, 7:26 PM
 */

#ifndef FUTARI_PARTICLES_H
#define FUTARI_PARTICLES_H

#include <iostream>
#include <limits>
#include <vector>

#include "core/rid.h"
#include "core/os/main_loop.h"
#include "scene/3d/visual_instance.h"
#include "scene/resources/material.h"
//#include "scene/3d/particles.h"

#include "futari_wind.h"
#include "futari_attractor.h"
#include "futari_vortex.h"
#include "futari_floor.h"
#include "futari_material.h"
#include "futari_server.h"

class FutariParticles : public GeometryInstance {
private:
    GDCLASS( FutariParticles, GeometryInstance );
    OBJ_CATEGORY( "3D" );

public:

    enum FutariDrawOrder {
        FDRAW_ORDER_INDEX,
        FDRAW_ORDER_LIFETIME,
        FDRAW_ORDER_VIEW_DEPTH,
    };

    enum {
        FMAX_DRAW_PASSES = 4
    };

    FutariParticles( );
    virtual ~FutariParticles( );

    //    Node* get_root( Node* ) const;
    bool find_node( const Node* stack, const Node* needle ) const;
    
    void _internal_process( );

    AABB get_aabb( ) const;
    PoolVector<Face3> get_faces( uint32_t p_usage_flags ) const;

    void set_emitting( bool p_emitting );
    void set_amount( int p_amount );
    void set_lifetime( float p_lifetime );
    void set_one_shot( bool p_one_shot );
    void set_pre_process_time( float p_time );
    void set_explosiveness_ratio( float p_ratio );
    void set_randomness_ratio( float p_ratio );
    void set_visibility_aabb( const AABB &p_aabb );
    void set_use_local_coordinates( bool p_enable );
    void set_process_material( const Ref<Material> &p_material );
    void set_speed_scale( float p_scale );

    bool is_emitting( ) const;
    int get_amount( ) const;
    float get_lifetime( ) const;
    bool get_one_shot( ) const;
    float get_pre_process_time( ) const;
    float get_explosiveness_ratio( ) const;
    float get_randomness_ratio( ) const;
    AABB get_visibility_aabb( ) const;
    bool get_use_local_coordinates( ) const;
    Ref<Material> get_process_material( ) const;
    float get_speed_scale( ) const;

    void set_fixed_fps( int p_count );
    int get_fixed_fps( ) const;

    void set_fractional_delta( bool p_enable );
    bool get_fractional_delta( ) const;

    void set_draw_order( FutariDrawOrder p_order );
    FutariDrawOrder get_draw_order( ) const;

    void set_draw_passes( int p_count );
    int get_draw_passes( ) const;

    void set_draw_pass_mesh( int p_pass, const Ref<Mesh> &p_mesh );
    Ref<Mesh> get_draw_pass_mesh( int p_pass ) const;

    virtual String get_configuration_warning( ) const;

    void restart( );

    AABB capture_aabb( ) const;

    void set_futari_layer(uint32_t p_mask);
    uint32_t get_futari_layer() const;

    void set_futari_layer_bit(int p_layer, bool p_enable);
    bool get_futari_layer_bit(int p_layer) const;

protected:

    static void _bind_methods( );
    void _notification( int p_notification );
    virtual void _validate_property( PropertyInfo &property ) const;

    Map<String, FutariModifier*> _mod_map;
    
    // called by _internal_process during server lock
    void _clear_modifier_vectors();
    bool _is_empty_modifier_vectors();
    void _rebuild_modifier_vectors();
    bool _validate_modifier_vectors();
    bool _update_modifier_vectors();
    
    void _regenerate_attractors( Ref<FutariMaterial>& );
    void _update_attractors_param( Ref<FutariMaterial>& );
    
    void _regenerate_winds( Ref<FutariMaterial>& );
    void _update_winds_param( Ref<FutariMaterial>& );
    
    void _regenerate_vortices( Ref<FutariMaterial>& );
    void _update_vortices_param( Ref<FutariMaterial>& );
    
    void _regenerate_floors( Ref<FutariMaterial>& );
    void _update_floors_param( Ref<FutariMaterial>& );
    
    Vector<FutariModifierData*> _modifiers;
    Vector<FutariAttractorData*> _attractors;
    Vector<FutariWindData*> _winds;
    Vector<FutariVortexData*> _vortices;
    Vector<FutariFloorData*> _floors;
    bool bypass_last_revision;
    uint64_t last_revision;
    
    bool _procmat_refresh;

    uint32_t futari_layers;

private:

    RID particles;

    bool one_shot;
    int amount;
    float lifetime;
    float pre_process_time;
    float explosiveness_ratio;
    float randomness_ratio;
    float speed_scale;
    AABB visibility_aabb;
    bool local_coords;
    int fixed_fps;
    bool fractional_delta;

    Ref<FutariMaterial> process_material;

    FutariDrawOrder draw_order;

    Vector<Ref<Mesh> > draw_passes;

};

VARIANT_ENUM_CAST( FutariParticles::FutariDrawOrder )

#endif /* FUTARI_PARTICLES_H */

