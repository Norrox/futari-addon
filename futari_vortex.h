/*
 *  
 *  
 *  _________ ____  .-. _________/ ____ .-. ____ 
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'      
 *  
 *  
 *  art & game engine
 *  
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *  
 * 
 *  This file is part of PROJECT_NAME
 *  For the latest info, see http://polymorph.cool/
 *  
 *  Copyright (c) 2019 polymorph.cool
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 *  ___________________________________( ^3^)_____________________________________
 *  
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *  
 * 
 */

/* 
 * File:   futari_vortex.h
 * Author: frankiezafe
 *
 * Created on January 11, 2019, 6:23 PM
 */

#ifndef FUTARI_VORTEX_H
#define FUTARI_VORTEX_H

#include "futari_modifier.h"

// required to avoid division by 0 in futari material shader!
#define FUTARI_VORTEX_MINRANGE 0.001
#define FUTARI_VORTEX_DEFAULT_BARYCENTRE 0.5
#define FUTARI_VORTEX_MINHEIGHT 0.001

class FutariVortexData : public FutariModifierData {
public:

    real_t height;

    bool changed_height;
    bool changed_top_tex;
    bool changed_bottom_tex;
    bool changed_height_tex;
    
    // textures
    Ref<GradientTexture> top_tex;
    Ref<GradientTexture> bottom_tex;
    Ref<GradientTexture> height_tex;

    FutariVortexData( ) :
    height( FUTARI_VORTEX_MINHEIGHT ),
    changed_height( false ),
    changed_top_tex( false ),
    changed_bottom_tex( false ),
    changed_height_tex( false )
    {
        range = FUTARI_VORTEX_MINRANGE;
    }

    void reset( ) {

        FutariModifierData::reset( );
        changed_height = false;
        changed_top_tex = false;
        changed_bottom_tex = false;
        changed_height_tex = false;

    }

    void operator=( const FutariVortexData& src ) {

        FutariModifierData::operator=( src );
        height = src.height;
        top_tex = src.top_tex;
        bottom_tex = src.bottom_tex;
        height_tex = src.height_tex;
        changed_height = src.changed_height;
        changed_top_tex = src.changed_top_tex;
        changed_bottom_tex = src.changed_bottom_tex;
        changed_height_tex = src.changed_height_tex;

    }

};

class FutariVortex : public FutariModifier {
    GDCLASS( FutariVortex, FutariModifier )

public:

    FutariVortex( );

    void set_range( real_t r );

    void set_height( real_t r );
    real_t get_height( ) const;

    void set_top_texture( const Ref<GradientTexture> &tex );
    Ref<GradientTexture> get_top_texture( ) const;

    void set_center_texture( const Ref<GradientTexture> &tex );
    Ref<GradientTexture> get_center_texture( ) const;
    
    void set_bottom_texture( const Ref<GradientTexture> &tex );
    Ref<GradientTexture> get_bottom_texture( ) const;
    
    void set_height_texture( const Ref<GradientTexture> &tex );
    Ref<GradientTexture> get_height_texture( ) const;

    FutariModifierData* data_ptr( );

protected:

    static void _bind_methods( );

    void refresh( );

    FutariVortexData _data;
    real_t height;

    Vector3 up;

    Ref<GradientTexture> top_texture;
    Ref<GradientTexture> bottom_texture;
    Ref<GradientTexture> height_texture;

private:

};

#endif /* FUTARI_VORTEX_H */

